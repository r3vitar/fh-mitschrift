# Einführung in die Informatik und Rechnerarchitektur

## Zahlensysteme

### Stellenschreibweise

N = (a(n-1) -> a(-m))r  
a(i) => Ziffer

`,` -> Komma -> `.` -> Decimal Point (Radix Punkt)

r = 10 => **Dezimalsystem**  
101(basis 10)  

> Def.: Ein Polyadisches Zahlensystem mit Basis `r` ist ein Zahlensystem in dem eine Zahl `N` nach Potenzen von `r` zerlegt wird

```101(basis 10) = 1 *100 + 0* 10 + 1 *1```
```101(basis 10) = 1* 10^2 + 0 *10^1 + 1* 10^0```

`r = 2, 101 = 1 * 2^2 + 0 * 2^1 + 1 * 2^0`

r = 2 => **Dualsystem** / Binärsystem(falsch -> können beliebige 2 Dinge sein)  

`a(n-1) * r^(n-1) + a(n-2) * r^(n-2)`

funktioniert genauso mit Nachkommastellen.  

n=1Σl=-m a(i)*r^2 = N(r)

r = 8 => Oktansystem

r10 | r2 | r8 | r16
---:|---:|---:|---:
 0 | 0 | 0 | 0
 1 | 1 | 1 | 1
 2 | 10 | 2 | 2
 3 | 11 | 3 | 3
 4 | 100 | 4 | 4
 5 | 101 | 5 | 5
 6 | 110 | 6 | 6
 7 | 111 | 7 | 7
 8 | 1000 | 10 | 8
 9 | 1001 | 11 | 9
 10 | 1010 | 12 | A
 11 | 1011 | 13 | B
 12 | 1100 | 14 | C
 13 | 1101 | 15 | D
 14 | 1110 | 16 | E
 15 | 1111 | 17 | F
 16 | 10000 | 20 | 10
 17 | 10001 | 21 | 11
 18 | 10010 | 22 | 12

 **BIT** = BINARY DIGIT

Potenz | 2^
------:|--:
0 | 1
1 | 2
2 | 4
3 | 8
4 | 16
5 | 32
6 | 64
7 | 128
8 | 256
9 | 512
10 | 1024
11 | 2048
12 | 4096
13 | 8182

Analoges Signal - zeit diskret, Amplitude diskret

![Abtastrate Diagramm](2021-09-17-17-19-55.png)
44100 kHz Abtastrate z.B.  
16 bit

___

`2^10 = 1024`  
`10^3 = 1000`

`2^20 = 1024 * 1024`  
`10^6 = 1000 * 1000`

`2^30 = 1024 * 1024 * 1024`  
`10^9 = 1000 * 1000 * 1000`

**ABBA** im Dezimalsystem

`A * 16^3 + B * 16^2 + B * 16^1 + A * 16^0`  
`10 * 16^3 + 11 * 16^2 + 11 * 16^1 + 10 * 16^0`
= `43962`
___

## Zahlenkonvertierungen

### Zahlen von Basis A nach Basis B umrechnen

B = 10

- aufstellen der Sumenformel
  - 101,01(2) nach (10)
    - `1 * 2^2 + 0 * 2^1 + 1 * 2^0 + 0 * 2^-1 + 1 * 2^-2`  = 5,25
  - FF(16) nach (10)
    - `F * 16^1 + F * 16^0` = `15 * 16 + 15 * 1` = `240 + 15` = `255`
- Rechnen in Basis **10**

### Zahlen von Basis B nach Basis A

#### Hornerschema

- Vorkommastellen
  - x10 = ?B
  - A = 10
  - = `(a(n-1) * r + a(n-2) + a2) * r + a1) *r + a0`

`1101(2) = 1 * 2^3 + 1 * 2^2 + 0 * 2^1 + 1 * 2^0`  
`((1 * 2 + 1) * 2 + 0) * 2 + 1`  
`13` => Dual  
`13/ 2 = 6` rest `1` = a0  
`6 / 2 = 3` rest `0` = a1  
`3 / 2 = 1` rest `1` = a2  
`1 / 2 = 0` rest `1` = a3  
= `1101`  

`100`(10) = `?`(16)  
`100 / 16 = 6` rest `4`  
`6 / 16 = 0` rest `6`  
= `64`(16)  

- Nachkommastellen
- **x^-1 * x^1 = 1** = x/x
- = `r^-1 * (a-1 + r^-1 * (a-2 + r^-1 *(a-3....)))`
- Beispiel:

`0.375`(10) = `?`(2)
`0.375 * 2 = 0.75`
`.75 * 2 = 1.5`
`.5 * 2 = 1.0`
= `0,011`

- Allgemein
  - A != 10 && B != 10
  - z.B.: 120(3) = ? (7)
    - Umweg über r=10
- SpezialFälle
  - A = B^k (k eine natürliche Zahl)
  - 2 => 16 (2^4 = 16)
    - 4er Blöcke
    - Tricks:
      - **A = 10 = 1010**
      - B = 10 = 1011
      - C = 10 = 1100
      - D = 10 = 1101
  - ABBA (16) => ?(2)
  - 1010 1011 1011 1010  

