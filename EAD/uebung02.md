# Uebung 02

## Uebung 02.01

### Code

```pascal
PROGRAM SameNumbers;
FUNCTION DifferentNumbersCount(num0, num1, num2: INTEGER): INTEGER;
BEGIN (* DifferentNumbersCount *)
  IF (num0 = num1) AND (num1 = num2) THEN BEGIN
    DifferentNumbersCount := 1;
  END ELSE IF (num0 = num1) OR (num0 = num2) OR (num1 = num2) THEN BEGIN
    DifferentNumbersCount := 2;
  END ELSE BEGIN
    DifferentNumbersCount := 3;  
  END;
END; (* DifferentNumbersCount *)
BEGIN (* SameNumbers *)
  WriteLn('Eingabe: ', 5, ', ', 7, ', ',9, ' = ', DifferentNumbersCount(5, 7, 9), ' verschiedene Nummern');
  WriteLn('Eingabe: ', 5, ', ', 7, ', ',5, ' = ', DifferentNumbersCount(5, 7, 5), ' verschiedene Nummern');
  WriteLn('Eingabe: ', 7, ', ', 7, ', ',7, ' = ', DifferentNumbersCount(7, 7, 7), ' verschiedene Nummern');
END. (* SameNumbers *)
```

<div style="page-break-after: always"></div>

## Uebung 02.02

### Code 

```pascal
PROGRAM PrimeCalculator;
uses DateUtils, sysutils;
VAR
  FromTime, ToTime: TDateTime;
  input, i: LongInt;
  result: Boolean;
  DiffMinutes: Integer;
FUNCTION IsPrime(num: LongInt): Boolean;
VAR
  prime: Boolean;
  i: LongInt;
BEGIN (* isPrime *)
  IF ((num > 1) AND (num <= 3) ) THEN BEGIN
    prime := true;
  END ELSE IF ((num <= 1) OR (num MOD 2 = 0)) THEN BEGIN
    prime := false;
  END ELSE BEGIN
    i:=3;
    prime := true;
    FOR i := 3 TO Round(Sqrt(num)) DO BEGIN
      IF (num MOD i = 0) THEN BEGIN
        prime := false;
        Break;
      END; (* IF *)
    END; (* FOR *) 
  END; (* IF *)
  IsPrime := prime;
END; (* isPrime *)
BEGIN (* PrimeCalculator *)
  input := 131071;
  FromTime := Now;
  FOR i := 0 TO input DO BEGIN
      result := IsPrime(i);
  END; (* FOR *)
  ToTime := Now;
  DiffMinutes := MilliSecondsBetween(ToTime,FromTime);
  WriteLn('ist ', input, ' eine primzahl? ', result);
  WriteLn(DiffMinutes);

END. (* PrimeCalculator *)
```

<div style="page-break-after: always"></div>


## Uebung 02.03

### Code

```pascal
PROGRAM Armstrong;
VAR input: INTEGER;
  FUNCTION Power(num, powerTo: INTEGER): LongInt;
    VAR
      i: INTEGER;
      result: LongInt;
    BEGIN
      result := num;
      IF (powerTo = 0) THEN BEGIN
        result := 1;
      END ELSE BEGIN
        FOR i := 1 TO powerTo DO BEGIN
          result := result * (num);
        END; (* FOR *)
      END; (* IF *)
      Power := result;
  END;
  FUNCTION CountDigits(num: LongInt): Integer;
  VAR
    i, count: Integer;
  BEGIN (* CountDigits *)
    count := 0;
    i := 0;
    WHILE count = 0 DO BEGIN
      IF (num div Power(10, i)) = 0 THEN BEGIN
        count := i+1;
      END ELSE BEGIN
        i:= i+1;
      END; (* IF *)
    END; (* WHILE *)
    CountDigits := count;
  END; (* CountDigits *)
  FUNCTION GetXNumber(num, x: LongInt): Integer;
  BEGIN (* GetXNumber *)
    GetXNumber := (num div Power(10, x-1)) mod 10;
  END; (* GetXNumber *)
  FUNCTION IsArmstrongNumber(num: LongInt): Boolean;
  VAR
    i, count: Integer;
    armstrong: LongInt;
  BEGIN (* IsArmstrongNumber *)
    count := CountDigits(num);
    armstrong := 0;
    FOR i := 0 TO count-1 DO BEGIN
      armstrong := armstrong + Power(GetXNumber(num, i), count-1);
    END; (* FOR *)
    IsArmstrongNumber := armstrong = num;
  END; (* IsArmstrongNumber *)
BEGIN (* Armstrong *)
  Read(input);
  IF (IsArmstrongNumber(input)) THEN BEGIN
    WriteLn(input, ' ist Armstrong');
  END ELSE BEGIN
    WriteLn(input, ' ist nicht Armstrong');
  END; (* IF *)
END. (* Armstrong *)
```